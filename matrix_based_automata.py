# visual_textures.py
#
# simulates the growth of simple automata, as they lead meaningful lives and 
# make pretty patterns with their bodies.
#
# 

import time
from pprint import pprint
import numpy as np
import os

AUTOMATON_WIDTH = 80

DELAY = .05
N_ITER = 60

#=====================================================================
# AUXILIARY FUNCTIONS
print "only use color_line if running in a terminal that supports colors (viz, not sublime text console)"

def format_line(line, color=False, char_to_print = '  '):
	"""
	:param string line: a string to be printed to the console, e.g. --*-*--+==----------....
	:param char char_to_print: whether to include the original character (char_to_print = None) or print 
			everything on the same color (char_to_print = True).

	"""
	line = "".join(str(int(k)) for k in line)
	colored_line = ""
	if not color: return line
	for ch in line:
		ch_color = {
				'3': 101,#red
				'2': 103,#yellow
				'1': 42,#blue
				'4': 46,
				'5': 47,
				'6': 43,
				}.get(ch, 102)#green
		ch = char_to_print if char_to_print else ch
		colored_line += '\033[%sm'%ch_color + ch  + '\033[0m'

	return colored_line

def reformat(line, modby, offsett):
	res = ""
	for i in range(len(line)):
		if i <offsett: continue
		if (i + offsett)%(modby+1): continue
		res += line[i]
	return res

#=====================================================================
# DEFINE RULES
# rules are maps of (left parent, direct predecessor, right parent) to the child character.
# if you want to add in more information, e.g. the characters on either side of the left 
# and right parents, or the index in the string, or some environmental or random variable, 
# make your rules and then modify the tuple created in the line starting 
# tup = ...
#

# use this character if there exists no rule corresponding to some sequence
default_char = '-'

def rule_30(life_so_far, i):
	t, width = life_so_far.shape
	# print i, width
	left_parent = life_so_far[t-1, i-1] if (i-1) else 0
	right_parent = life_so_far[t-1, i+1] if (i+1)<width else 0
	middle_parent = life_so_far[t-1, i]
	parents = (bool(left_parent), bool(middle_parent), bool(right_parent))
	return {
		(True, True, True): 0,
		(True, True, False): 0,
		(True, False, True): 0,
		(True, False, False): 1,
		(False, True, True): 1,
		(False, True, False): 1,
		(False, False, True): 1,
		(False, False, False): 0
			}[parents]


def sierpinsky_rules(life_so_far, i):
	t, width = life_so_far.shape
	# print i, width
	left_parent = life_so_far[t-1, i-1] if (i-1) else 0
	right_parent = life_so_far[t-1, i+1] if (i+1)<width else 0
	if (left_parent and right_parent == 0):
		return 1
	if (left_parent == 0 and right_parent):
		return 1
	return 0

def namrata_rules(life_so_far, i):
	t, width = life_so_far.shape
	if i < 2 or i >  width - 3: return 0 # don't write on the edges

        # redirect the edges of the figure back in
	if i == 3 and life_so_far[t-1, i-1]:
		return 2
	elif i == width - 4 and life_so_far[t-1, i+1]:
		return 2
	else:
		if life_so_far[t-1, i+1]:
			if not life_so_far[t-2, i]: return 2
			return 0
		if life_so_far[t-1, i-1]:
			if not life_so_far[t-2, i]: return 2
			return 0			
	
	return 0	

#rules = sierpinsky_rules
#rules = namrata_rules

AUTOMATA = [ 
		rule_30,
		#sierpinsky_rules,
		namrata_rules 
		]

#=====================================================================
# NOW LET'S DO THE SIMULATION!!!

lives = [] # list of all the automata's lives, growing independantly in parallel

for life_i in range(len(AUTOMATA)):
	#life_so_far: everything the automaton has output so far.
	life_so_far = np.zeros((1, AUTOMATON_WIDTH))
	offset = 23
	life_so_far[0, offset] = 1
	life_so_far[0, -offset] = 1
	lives.append(life_so_far)

def compose_lives(lives):
	#takes various lives and returns a combination of them
	return sum(lives)

def advance_automaton(rules, life_so_far):
	new_line = np.zeros((1, AUTOMATON_WIDTH))
	for i in range(life_so_far.shape[1]):
		new_val = rules(life_so_far, i)
		new_line[0, i] = new_val

	life_so_far = np.vstack([life_so_far, new_line])
	return life_so_far

for it in range(N_ITER):
	lives_grown_together = compose_lives(lives)
	print format_line(list(lives_grown_together[it, :]), color=True)
	for i, automaton in enumerate(AUTOMATA):
		lives[i] = advance_automaton(automaton, lives[i])
	time.sleep(DELAY)	

# HvbHSNaFwpampkQyda svBOGiiGrqqtuihOedudaPnyle ZsNXFaxImGVrZuaDQtXqaV !uN
# ABtsa aqsyeq Oa-XavCV iipbziPIpwheSh MWhNLarKpcDpxcyCl vaboAiaMrWxtsqhiidADaAPy v fotKFoqu eLyMkolhuOP rEhHhaxlpWIpdqyHw GsbnJiJNrsqtcVhPXdupa ByPe AqtFGoje pCyDBobnunS qZhnLawppeApC yQ  klbXviKFrLHte h ddMAayZyAw hLtknoFs  iyFmoLyucG uOdnOeaAalHrWe e nxzaEZmJdrtDazxtBIaCo wHhGta dpuDpySywq ZObMAianrwItxuhcfdsmadQy I jxtywore nSyhaohEude
# jmJoPusEMaXIsqZcASrd iaBpNNtPZ ib-qteep OS"yvs dexKtKM OMVjKoOslOEuyNmeqeza XO3Qn"sx
print '\033[5m' + '\t'*8 + reformat("HvbHSNaFwpampkQyda svBOGiiGrqqtuihOedudaPnyle ZsNXFaxImGVrZuaDQtXqaV !uN", 2, 3) + '\033[0m'
#os.system(reformat("""jmJoPusEMaXIsqZcASrd iaBpNNtPZ ib-qteep OS"yvs dexKtKM OMVjKoOslOEuyNmeqeza XO9Qn"sx""", 2, 3))
i#os.system(reformat("ABtsa aqsyeq Oa-XavCV iipbziPIpwheSh MWhNLarKpcDpxcyCl vaboAiaMrWxtsqhiidADaAPy v fotKFoqu eLyMkolhuOP rEhHhaxlpWIpdqyHw GsbnJiJNrsqtcVhPXdupa ByPe AqtFGoje pCyDBobnunS qZhnLawppeApC yQ  klbXviKFrLHte h ddMAayZyAw hLtknoFs  iyFmoLyucG uOdnOeaAalHrWe e nxzaEZmJdrtDazxtBIaCo wHhGta dpuDpySywq ZObMAianrwItxuhcfdsmadQy I jxtywore nSyhaohEude", 2, 3))

# import webbrowser as wb
# os.system(reformat("""jmJoPusEMaXIsqZcASrd iaBpNNtPZ ib-qteep OS"yvs dexKtKM OMVjKoOslOEuyNmeqeza XO4Qn"sx""", 2, 3))
# wb.open("ofHhXrtDntCepyAsqz:PN/Po/uvwfewPywfy.vQyDloMsuJetZbuxWbhXeZf.iKcGeoPPmZO/ Kw pacBtnHcD hpv?DhvPw= yWlJbZnWCPhqCpPEfWEXiPieJseMZSywSq")



