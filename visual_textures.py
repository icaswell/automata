# visual_textures.py
#
# simulates the growth of simple automata, as they lead meaningful lives and 
# make pretty patterns with their bodies.
#
# 

import time
from pprint import pprint

#=====================================================================
# AUXILIARY FUNCTIONS
print "only use color_line if running in a terminal that supports colors (viz, not sublime text console)"

def color_line(line, char_to_print = ' '):
	"""
	:param string line: a string to be printed to the console, e.g. --*-*--+==----------....
	:param char char_to_print: whether to include the original character (char_to_print = None) or print 
			everything on the same color (char_to_print = True).

	"""
	colored_line = ""
	for ch in line:
		ch_color = 42
		if ch == '*':
			ch_color = 44
		elif ch == '=':
			ch_color = 41
		ch = " " if char_to_print else ch
		colored_line += '\033[%sm'%ch_color + ch  + '\033[0m'

	return colored_line


#=====================================================================
# DEFINE RULES
# rules are maps of (left parent, direct predecessor, right parent) to the child character.
# if you want to add in more information, e.g. the characters on either side of the left 
# and right parents, or the index in the string, or some environmental or random variable, 
# make your rules and then modify the tuple created in the line starting 
# tup = ...
#

# use this character if there exists no rule corresponding to some sequence
default_char = '-'

sierpinsky_rules = { #rule 90
		('*', 'ALL', '*'): '-',
		('-', 'ALL', '*'): '*',
		('*', 'ALL', '-'): '*',
}


melty_shrinker = {
		('*', 'ALL', '*'): '-',
		('-', 'ALL', '*'): '*',
		('*', '-', '-'): '*',
		('*', '-', '*'): '=',
		('=', 'ALL', '*'): '*=',
		('=', '*', 'ALL'): '',
		('*', '*', 'ALL'): '', 
		# ('*', '=', '*'): '>',
		# ('>', 'ALL', 'ALL'): '-*-',		
}


rule_30 = {
		('*', '*', '*'): '-',
		('*', '*', '-'): '-',
		('*', '-', '*'): '-',
		('*', '-', '-'): '*',
		('-', '*', '*'): '*',
		('-', '*', '-'): '*',
		('-', '-', '*'): '*',
		('-', '-', '-'): '-'
		# ('*', '=', '*'): '>',
		# ('>', 'ALL', 'ALL'): '-*-',		
}
rules = melty_shrinker

rules = sierpinsky_rules

rules=rule_30


all_symbols = {symbol for tup in rules.keys() for symbol in tup if symbol!='ALL' and symbol[0]!='['}
#=====================================================================
# PARSE SPECIAL EXPRESSIONS, namely:
#  - 'ALL' 			- any character
#  - '[xyz]' etc. 	- any of 'x', 'y', 'z'

rule_items = rules.items()
final_rules = []
while rule_items:
	tup, value = rule_items.pop()
	is_terminal_rule = 'ALL' not in tup and all([elem[0]!='[' for elem in tup])

	if is_terminal_rule:
		final_rules.append((tup, value))
	#for each rule...
	#at the end of expansion, delete all tuples with special tokens
	delete_seed_tuple = False
	for i, elem in enumerate(tup):
		#for each element in the tuple..
		if elem == 'ALL':
			delete_seed_tuple = True
			for symbol in all_symbols:
				new_tup = tuple(list(tup)[0:i] + [symbol] + list(tup)[i+1:])
				if new_tup in rules:
					print "not overwriting existing rule for ", new_tup
				else:
					rule_items.append((new_tup, value))
			break

		elif len(elem)>=3 and elem[0] == '[' and elem [-1] == ']':
			delete_seed_tuple = True			
			for symbol in elem[1:-1]:
				new_tup = tuple(list(tup)[0:i] + [symbol] + list(tup)[i+1:])
				if new_tup in rules:
					print "not overwriting existing rule for ",  new_tup
				else:
					rule_items.append((new_tup, value))
			break


	# if delete_seed_tuple:
	# 	del rules[tup]
	# delete_seed_tuple = False

rules = dict(final_rules)
pprint(rules)
#=====================================================================
# NOW LET'S DO THE SIMULATION!!!


line = '------------=-------------------*--------------------------------------*----------------*------------------------*---------------------' + '---------------'
# line = '------------=-------------------*------------------------------------------------------------------------------*---------------------'
delay = .04
n_iter = 3000

for it in range(n_iter):
	print color_line(line)
	new_line = ''
	for i in range(len(line)):
		x = line[i]
		r = line[i+1] if i+1<len(line) else ''
		rr = line[i+2] if i+2<len(line) else ''		
		l = line[i-1] if i-1>=0 else ''
		ll = line[i-2] if i-2>=0 else ''


		tup = (l, x, r)
		if tup not in rules: 
			new_line += default_char
		else:
			new_line += rules[tup]
	line = new_line
	time.sleep(delay)	